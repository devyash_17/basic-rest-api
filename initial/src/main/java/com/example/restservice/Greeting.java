package com.example.restservice;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Greeting {

    @JsonProperty("id")
    private final long id;
    @JsonProperty("name")
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}