package com.example.restservice;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();
    private static List<Greeting> greetings = new ArrayList<>();

    @GetMapping("/greetings")
    public List<Greeting> getGreetings()
    {
        return greetings;
    }

    @PostMapping("/add")
    public List<Greeting> greeting(@RequestBody Greeting g) {
        greetings.add(g);
        return greetings;
    }
}